<?php

namespace app\controllers;

class BaseController extends \yii\web\Controller
{
	public function beforeAction($action)
    {
        if(!parent::beforeAction($action))
            return false;

		\Yii::$app->language = \Yii::$app->user->isGuest ? 'fr-FR' : \Yii::$app->user->identity->getLanguage()->locale;
        return true ;
    }
}

?>