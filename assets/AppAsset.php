<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

    // Constructor
    public function __construct() 
    {
        // Get the app environment
        $environment = \Yii::$app->params['environment'];
        
        // Set dev assets
        if( $environment == 'dev' ) 
        {
            array_unshift($this->css, 'https://unpkg.com/buefy@0.8.2/dist/buefy.css');
            array_unshift($this->css, 'https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.css');
            
            array_push($this->js, 'https://cdn.jsdelivr.net/npm/vue/dist/vue.js');
            array_push($this->js, 'https://unpkg.com/buefy/dist/buefy.js');
        }
        // Set prod assets
        else if( $environment == 'prod' ) 
        {
            array_unshift($this->css, 'https://unpkg.com/buefy/dist/buefy.min.css');
            array_unshift($this->css, 'https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css');
            array_push($this->js, 'https://cdn.jsdelivr.net/npm/vue');
            array_push($this->js, 'https://unpkg.com/buefy/dist/buefy.min.js');
        }
        else
            throw new \yii\base\UserException("Wrong environment mode");
    }
}
