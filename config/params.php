<?php

$env = require __DIR__ . '/env.php';

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'environment' => $env,
];
